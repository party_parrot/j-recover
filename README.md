# J-ReCoVer   

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

J-ReCoVer, short for "Java Reducer Commutativity Verifier", is a verification tool for Map-Reduce program written in Java.
J-ReCoVer symbolic executes the function body of a reducer and tries to prove that the order of the inputs will not
affect the output.
For more details please visit [our website](http://www.jrecover.tk).

## Installation

Run ```./install.sh``` to build and test J-ReCoVer.

```j-ReCover``` performs the same function as our [online version](http://www.jrecover.tk).
The script will try to compile your reducer, determine commutativity,
and find counterexamples.

```j-recover.jar``` is the core engine of our tool. It compiles your MapReduce program into an executable jar and analyzes
it.


## Usage
```
$ ./j-ReCoVer [reducer].java
Example:
 $ ./j-ReCoVer sample_reducer.java
```


## Build j-recover.jar without scripts
### Using Ant
```
$ ant
```
### Using eclipse
* Import the project into your workspace
* Include all jar files in `/lib` from `Project > Properties > Java Build Path > Libraries > Add External JARs...`
